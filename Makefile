all: bin/static bin/dinamic
	echo "bin/estatic bin/dinamic fueron creados"

bin/static: lib/libstatic.a obj/main.o obj/dias.o obj/segundos.o
	gcc -Wall -static -L .lib/libstatic obj/main.o obj/dias.o obj/segundos.o -o bin/static

bin/dinamic: lib/libdinamic.so obj/main.o obj/dias.o obj/segundos.o
	gcc -Wall -L .lib/libdinamic obj/main.o obj/dias.o obj/segundos.o -o bin/dinamic

lib/libstatic.a: obj/dias.o obj/segundos.o
	ar rcs lib/libstatic.a obj/dias.o obj/segundos.o

lib/libdinamic.so: obj/dias.o obj/segundos.o
	gcc -shared -fPIC obj/dias.o obj/segundos.o -o lib/libdinamic.so
	export LD_LIBRARY_PATH=libdinamic:$LD_LIBRARY_PATH

obj/main.o: src/main.c
	gcc -Wall -c src/main.c -o obj/main.o

obj/dias.o: src/dias.c
	gcc -Wall -c src/dias.c -o obj/dias.o

obj/segundos.o: src/segundos.c
	gcc -Wall -c src/segundos.c -o obj/segundos.o

clean:
	rm bin/* obj/* lib/*


